"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sequence = 1;
var TODO_DATA = [
    { id: sequence++, label: 'Ecrire un article', checked: true },
    { id: sequence++, label: 'Veille techno sur TypeScript', checked: false },
    { id: sequence++, label: 'Voir la dernière release de ExpressJS', checked: false }
];
var TodoDao = /** @class */ (function () {
    function TodoDao() {
    }
    TodoDao.prototype.findAll = function () {
        return TODO_DATA;
    };
    TodoDao.prototype.findById = function (id) {
        var todos = TODO_DATA.filter(function (elt) { return elt.id == id; });
        console.log(id, todos, TODO_DATA);
        return todos.length > 0 ? todos[0] : undefined;
    };
    TodoDao.prototype.save = function (todo) {
        var updated = this.findById(todo.id) || { id: ++sequence };
        updated.label = todo.label;
        updated.checked = todo.checked !== undefined;
        if (!TODO_DATA.find(function (elt) { return elt.id === todo.id; })) {
            TODO_DATA.push(updated);
        }
        return updated;
    };
    TodoDao.prototype.deleteById = function (id) {
        var index = TODO_DATA.map(function (elt) { return elt.id; }).indexOf(id);
        if (index >= 0) {
            TODO_DATA.splice(index, 1);
        }
    };
    return TodoDao;
}());
exports.TodoDao = TodoDao;
//# sourceMappingURL=todo.dao.js.map