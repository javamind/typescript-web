"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var handlebars = require("express-handlebars");
var errorHandler = require("errorhandler");
var todo_route_1 = require("./todo.route");
var bodyParser = require("body-parser");
var Express = /** @class */ (function () {
    function Express(options) {
        this.options = options;
        //create expressjs application
        this.app = express();
        //configure application
        this.config();
        // 404 redirection
        this.app.all('*', function (req, res) { return res.redirect("/404.html"); });
        this.app.set('port', this.options.port);
    }
    /**
     * Configure application
     */
    Express.prototype.config = function () {
        var router = express.Router();
        todo_route_1.TodoRoute.create(router);
        this.app
            .engine('handlebars', handlebars())
            .set('view engine', 'handlebars')
            .enable('view cache')
            .enable('trust proxy')
            .set('views', __dirname + "/../src/main/views/")
            .use(bodyParser())
            .use(router)
            .use(express.static(this.options.static))
            .get('/', function (req, res) { return res.send('Hello World!'); })
            .use(errorHandler());
    };
    /**
     * Bootstrap the application.
     */
    Express.bootstrap = function (options) {
        console.log("Try to start server");
        return new Express(options);
    };
    return Express;
}());
exports.Express = Express;
//# sourceMappingURL=express.js.map