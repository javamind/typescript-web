"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var errorHandler = require("errorhandler");
var Express = /** @class */ (function () {
    function Express(options) {
        this.options = options;
        //create expressjs application
        this.app = express();
        //configure application
        this.config();
        // 404 redirection
        //this.app.all('*', (req, res) => res.redirect(`/404.html`));
        this.app.set('port', this.options.port);
    }
    /**
     * Configure application
     */
    Express.prototype.config = function () {
        this.app;
        // .engine('handlebars', handlebars())
        // .set('view engine', 'handlebars')
        // .enable('view cache')
        // .enable('trust proxy')
        // .set('views', `${__dirname}/views/`)
        //error handling
        this.app
            .get('/', function (req, res) { return res.send('Hello World!'); })
            .use(errorHandler());
    };
    /**
     * Bootstrap the application.
     */
    Express.bootstrap = function (options) {
        console.log("Try to start server");
        return new Express(options);
    };
    return Express;
}());
exports.Express = Express;
//# sourceMappingURL=express.js.map