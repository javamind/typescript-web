"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("./express");
var http = require("http");
var options = {
    static: "src/main/static",
    port: 8081
};
var server = express_1.Express.bootstrap(options).app;
http.createServer(server)
    .listen(options.port)
    .on("listening", function () { return console.debug('Listening on ' + options.port); });
//# sourceMappingURL=app.js.map