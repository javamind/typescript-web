"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var todo_dao_1 = require("./todo.dao");
var TodoRoute = /** @class */ (function () {
    function TodoRoute(dao) {
        this.dao = dao;
    }
    TodoRoute.create = function (router) {
        var route = new TodoRoute(new todo_dao_1.TodoDao());
        router.get("/todos", function (req, res) { return route.findAll(req, res); });
        router.get("/todos/:id", function (req, res) { return route.findById(req, res, req.params.id); });
        router.post("/todos/:id/delete", function (req, res) { return route.deleteById(req, res, req.params.id); });
        router.get("/todos/create", function (req, res) { return route.findById(req, res, undefined); });
        router.post("/todos", function (req, res) { return route.save(req, res, req.body); });
    };
    TodoRoute.prototype.viewOne = function (req, res, todo) {
        res.locals['title'] = todo.id ? 'Modification Todo' : 'Ajout Todo';
        res.locals['todo'] = this.dao.findById(todo.id) || {};
        res.render('todo_view');
    };
    TodoRoute.prototype.viewAll = function (req, res, todos) {
        res.locals['title'] = 'Liste des todos';
        res.locals['todos'] = todos;
        res.render('todo_list_view');
    };
    TodoRoute.prototype.findAll = function (req, res) {
        this.viewAll(req, res, this.dao.findAll());
    };
    TodoRoute.prototype.findById = function (req, res, id) {
        this.viewOne(req, res, this.dao.findById(id) || {});
    };
    TodoRoute.prototype.deleteById = function (req, res, id) {
        this.dao.deleteById(id);
        this.findAll(req, res);
    };
    TodoRoute.prototype.save = function (req, res, todo) {
        if (!todo.label) {
            res.locals['errors'] = {
                has: true,
                label: 'Le libellé est obligatoire'
            };
            this.viewOne(req, res, todo);
        }
        else {
            this.dao.save(todo);
            this.findAll(req, res);
        }
    };
    return TodoRoute;
}());
exports.TodoRoute = TodoRoute;
//# sourceMappingURL=todo.route.js.map