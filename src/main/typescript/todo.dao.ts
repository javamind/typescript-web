export interface Todo {
    id: number;
    label: string;
    checked: boolean;
}

let sequence = 1;
const TODO_DATA: Array<Todo> = [
    {id: sequence++, label: 'Ecrire un article', checked: true},
    {id: sequence++, label: 'Veille techno sur TypeScript', checked: false},
    {id: sequence++, label: 'Voir la dernière release de ExpressJS', checked: false}
];

export class TodoDao {

    findAll(): Array<Todo> {
        return TODO_DATA;
    }

    findById(id: number): Todo {
        const todos = TODO_DATA.filter(elt => elt.id == id);
        return todos.length > 0 ? todos[0] : undefined;
    }

    save(todo: Todo): Todo {
        const updated: Todo = this.findById(todo.id) || {id: ++sequence} as Todo;
        updated.label = todo.label;
        updated.checked = todo.checked !== undefined;
        if (!TODO_DATA.find(elt => elt.id === todo.id)){
            TODO_DATA.push(updated);
        }
        return updated;
    }

    deleteById(id: number) {
        const index = TODO_DATA.map(elt => elt.id).indexOf(id);
        if (index >= 0) {
            TODO_DATA.splice(index, 1);
        }
    }
}