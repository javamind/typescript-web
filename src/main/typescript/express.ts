import * as express from 'express';
import * as handlebars from 'express-handlebars';
import errorHandler = require("errorhandler");
import {TodoRoute} from "./todo.route";
import bodyParser = require("body-parser");

export interface ServerOptions {
    // Path contenant toutes les ressources statiques (css, js...)
    static: string;
    // Port de votre serveur
    port: number;
}

export class Express {

    public app: express.Application;

    constructor(public options: ServerOptions) {

        //create expressjs application
        this.app = express();

        //configure application
        this.config();

        // 404 redirection
        this.app.all('*', (req, res) => res.redirect(`/404.html`));

        this.app.set('port', this.options.port);
    }

    /**
     * Configure application
     */
    public config() {
        const router = express.Router();

        TodoRoute.create(router);

        this.app
            .engine('handlebars', handlebars())
            .set('view engine', 'handlebars')
            .enable('view cache')
            .enable('trust proxy')
            .set('views', `${__dirname}/../src/main/views/`)
            .use(bodyParser())
            .use(router)
            .use(express.static(this.options.static))
            .get('/', (req, res) => res.send('Hello World!'))
            .use(errorHandler());
    }

    /**
     * Bootstrap the application.
     */
    public static bootstrap(options: ServerOptions): Express {
        console.log("Try to start server");
        return new Express(options);
    }

}
