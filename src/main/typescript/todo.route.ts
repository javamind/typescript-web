import {Request, Response, Router} from "express";
import {Todo, TodoDao} from "./todo.dao";


export class TodoRoute{

    dao:TodoDao;
    
    constructor(dao: TodoDao) {
        this.dao = dao;
    }

    public static create(router: Router) {
        const route = new TodoRoute(new TodoDao());
        router.get("/todos", (req: Request, res: Response) => route.findAll(req, res));
        router.get("/todos/:id", (req: Request, res: Response) => route.findById(req, res, (req.params as any).id));
        router.post("/todos/:id/delete", (req: Request, res: Response) => route.deleteById(req, res, (req.params as any).id));
        router.get("/todos/create", (req: Request, res: Response) => route.findById(req, res, undefined));
        router.post("/todos", (req: Request, res: Response) => route.save(req, res, req.body));
    }

    private viewOne(req: Request, res: Response, todo:Todo){
        res.locals['title'] = todo.id ? 'Modification Todo' : 'Ajout Todo';
        res.locals['todo'] = this.dao.findById(todo.id) || {} as Todo;
        res.render('todo_view');
    }

    private viewAll(req: Request, res: Response, todos:Array<Todo>){
        res.locals['title'] = 'Liste des todos';
        res.locals['todos'] = todos;
        res.render('todo_list_view');
    }

    private findAll(req: Request, res: Response) {
        this.viewAll(req, res, this.dao.findAll());
    }

    private findById(req: Request, res: Response, id: number) {
        this.viewOne(req, res, this.dao.findById(id) || {} as Todo);
    }

    private deleteById(req: Request, res: Response, id: number) {
        this.dao.deleteById(id);
        this.findAll(req, res);
    }

    private save(req: Request, res: Response, todo: Todo) {
        if(!todo.label){
            res.locals['errors'] = {
                has: true,
                label: 'Le libellé est obligatoire'
            };
            this.viewOne(req, res, todo);
        }
        else{
            this.dao.save(todo);
            this.findAll(req, res);
        }
    }
}